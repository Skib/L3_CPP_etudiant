#ifndef FIBONACCI_HPP
#define FIBONACCI_HPP

int fibonacciRecursif(int n);
int fibonacciIteratif(int n);

#endif
