#include <iostream>
#include "Fibonacci.hpp"

int fibonacciRecursif(int n)
{
	if (n==0)
	return 0;
	if (n==1)
	return 1;
	return fibonacciRecursif(n-1) + fibonacciRecursif(n-2);
}

int fibonacciIteratif(int n)
{
	if (n==0 || n==1)
	return n;
	int a=0, b=1, tmp;
	for (int i=1 ; i<n ; i++)
	{
		tmp=b;
		b+=a;
		a=tmp;
	}
	return b;
}

		
	
